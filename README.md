Apache Cordova WebSocket Plugin for Android
=========================

Prerequisites:
- Apache Maven
- Apache Cordova 2.8.0


Include the Apache Cordova jar in the local repository using:

>mvn install:install-file \
-Dfile=cordova-2.8.0.jar \
-DgroupId=org.apache.cordova \
-DartifactId=phonegap \
-Dversion=2.8.0 \
-Dpackaging=jar

Create the package:

>mvn package





