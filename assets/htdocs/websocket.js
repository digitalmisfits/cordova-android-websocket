(function () {

	var log = function(d) { if(console && console.log) { console.log(d); }};
    var global = window;
    var self
    
    var WebSocket = global.WebSocket = function (url) {
	
        self = this
        self.readyState = null;

        Cordova.exec(
			function(id) {
				self.id = id;
				self.readyStateInterval = window.setInterval(function() {
			        Cordova.exec(function(readyState) { self.readyState = readyState; }, null,
			            'WebSocket', 'getReadyState', [self.id]
			        );
				}, 500);
				WebSocket.store[self.id] = self;
        		Cordova.exec(null, null, 'WebSocket', 'connect', [ self.id ]);				
			}, null,
            'WebSocket', 'create', [url]
       	);
    };

    WebSocket.store = {};
    
    WebSocket.onmessage = function (evt) {
        WebSocket.store[evt._target]['onmessage'].call(global, evt);
    }

    WebSocket.onopen = function (evt) {
        WebSocket.store[evt._target]['onopen'].call(global, evt);
    }

    WebSocket.onclose = function (evt) {
        WebSocket.store[evt._target]['onclose'].call(global, evt);
    }

    WebSocket.onerror = function (evt) {
        WebSocket.store[evt._target]['onerror'].call(global, evt);
    }

    WebSocket.prototype.send = function (data) {
        Cordova.exec(null, null,
            'WebSocket', 'send', [ self.id, data ]
        );
    }

    WebSocket.prototype.close = function () {
        Cordova.exec(null, null, 
        	'WebSocket', 'close', [ self.id ]
		);
    }

    WebSocket.prototype.getReadyState = function () {
		return self.readyState;
    }

    WebSocket.prototype.onopen = function (evt) {
        throw new Error('onopen not implemented.');
    };

    WebSocket.prototype.onmessage = function (evt) {
        throw new Error('onmessage not implemented.');
    };

    WebSocket.prototype.onerror = function (evt) {
        throw new Error('onerror not implemented.');
    };

    WebSocket.prototype.onclose = function (evt) {
        throw new Error('onclose not implemented.');
    };
})();