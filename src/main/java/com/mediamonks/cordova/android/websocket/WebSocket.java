package com.mediamonks.cordova.android.websocket;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.java_websocket.drafts.Draft_17;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class WebSocket extends CordovaPlugin {

	private Map<String, WebSocketClient> sockets;
	private final static String ACTION_CREATE = "create";
	private final static String ACTION_CONNECT = "connect";
	private final static String ACTION_SEND = "send";
	private final static String ACTION_CLOSE = "close";
	private final static String ACTION_GET_READY_STATE = "getReadyState";
	
	private final static String TAG = "WebSocket";

	public WebSocket() {
		super();
		sockets = new HashMap<String, WebSocketClient>();
	}

	public boolean isSynch(String action) {
		if (action.equals(ACTION_GET_READY_STATE) || action.equals(ACTION_CREATE)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		
		Log.d(TAG, String.format("Method: %s, action: %s, params: %s", "execute", action, args.toString()));
		
		try {
			if (action.equals(ACTION_CREATE)) {
				WebSocketClient webSocketClient = new WebSocketClient(webView, new URI(args.getString(0)), new Draft_17(),
						generateUniqueId());
				sockets.put(webSocketClient.getId(), webSocketClient);
				callbackContext.success(webSocketClient.getId());
				return true;
			} else if (action.equals(ACTION_CONNECT)) {
				sockets.get(args.getString(0)).connect();
				callbackContext.success();
				return true;
			} else if (action.equals(ACTION_SEND)) {
				sockets.get(args.getString(0)).send(args.getString(1));
				callbackContext.success();
				return true;
			} else if (action.equals(ACTION_CLOSE)) {
				sockets.get(args.getString(0)).close();
				callbackContext.success();
				return true;
			} else if (action.equals(ACTION_GET_READY_STATE)) {
				callbackContext.success(sockets.get(args.getString(0)).getReadyState().toString());
				return true;
			}

		} catch (Exception e) {
			Log.d("WebSockets", e.getMessage());
			throw new RuntimeException(e);
		}

		return false;
	}

	private String generateUniqueId() {
		return "WEBSOCKET." + new Random().nextInt(100);
	}
}
