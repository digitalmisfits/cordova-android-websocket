package com.mediamonks.cordova.android.websocket.example;

import java.net.InetSocketAddress;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

public class WebSocketServerImpl extends WebSocketServer {

	public WebSocketServerImpl(InetSocketAddress address) {
		super(address);
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		System.out.println("new connection to " + conn.getRemoteSocketAddress());
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		System.out.println("closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		System.out.println("received message form " + conn.getRemoteSocketAddress() + ": " + message);
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		System.out.println("an error occured on connection " + conn + ":" + ex);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		WebSocketServerImpl ws = new WebSocketServerImpl(new InetSocketAddress("localhost", 8080));
		ws.start();
	}
}
