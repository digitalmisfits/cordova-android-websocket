package com.mediamonks.cordova.android.websocket.example;

import java.net.URI;
import java.net.URISyntaxException;

import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

public class WebSocketClientImpl extends org.java_websocket.client.WebSocketClient {
	
	private static final String echo = "ws://10.0.2.2:8080";

	public WebSocketClientImpl(URI serverUri) {
		super(serverUri, new Draft_17());
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
		System.out.println("onOpen");
	}

	@Override
	public void onMessage(String message) {
		System.out.println(message);
		
	}

	@Override
	public void onClose(int code, String reason, boolean remote) {
		System.out.println("onClose");
	}

	@Override
	public void onError(Exception ex) {
		System.out.println("onError");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		WebSocketClientImpl ws = null;
		
		try {
			ws = new WebSocketClientImpl(new URI(echo));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		ws.connect();
	}
}
