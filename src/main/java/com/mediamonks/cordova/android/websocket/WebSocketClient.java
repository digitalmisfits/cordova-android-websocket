package com.mediamonks.cordova.android.websocket;

import android.webkit.WebView;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;

public class WebSocketClient extends org.java_websocket.client.WebSocketClient {

	private static String BLANK_MESSAGE = "";
	private static String EVENT_ON_OPEN = "onopen";
	private static String EVENT_ON_MESSAGE = "onmessage";
	private static String EVENT_ON_CLOSE = "onclose";
	private static String EVENT_ON_ERROR = "onerror";

	private WebView appView;
	private String id;

	public String getId() {
		return id;
	}

	public WebSocketClient(WebView appView, URI serverUri, Draft draft,
			String id) {
		super(serverUri, draft);
		this.appView = appView;
		this.id = id;
	}

	private WebSocketClient(URI serverUri, Draft draft) {
		super(serverUri, draft);
	}

	private WebSocketClient(URI serverURI) {
		super(serverURI);
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
		appView.post(new Runnable() {
			public void run() {
				appView.loadUrl(buildJavaScriptData(EVENT_ON_OPEN,
						BLANK_MESSAGE));
			}
		});
	}

	@Override
	public void onMessage(final String message) {
		appView.post(new Runnable() {
			public void run() {
				String sanitizedMessage = message.replace("\n", "\\n").replace(
						"\0", "\\0");
				appView.loadUrl(buildJavaScriptData(EVENT_ON_MESSAGE,
						sanitizedMessage));
			}
		});
	}

	@Override
	public void onClose(int code, final String reason, boolean remote) {
		appView.post(new Runnable() {
			public void run() {
				appView.loadUrl(buildJavaScriptData(EVENT_ON_CLOSE,
						reason));
			}
		});
	}

	@Override
	public void onError(final Exception ex) {
		appView.post(new Runnable() {
			public void run() {
				appView.loadUrl(buildJavaScriptData(EVENT_ON_ERROR,
						ex.getMessage()));
			}
		});
	}

	/**
	 * Builds text for javascript engine to invoke proper event method with
	 * proper data.
	 * 
	 * @param event
	 *            Websocket event (onOpen, onMessage etc.)
	 * @param msg
	 *            Text message received from websocket server
	 * 
	 * @return
	 */
	private String buildJavaScriptData(String event, String msg) {
		String _d = "javascript:WebSocket." + event + "(" + "{"
				+ "\"_target\":\"" + id + "\"," + "\"data\":'" + msg + "'"
				+ "}" + ")";
		return _d;
	}
}